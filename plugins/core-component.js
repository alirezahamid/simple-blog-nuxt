import Vue from 'vue'

import PostsCard from '@/components/Posts/PostsCard.vue'
import PostsCarousel from '@/components/Posts/PostsCarousel.vue'

Vue.component('PostsCard', PostsCard)
Vue.component('PostsCarousel', PostsCarousel)
