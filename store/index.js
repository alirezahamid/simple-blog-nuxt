import Vuex from 'vuex'
import Cookie from "js-cookie";

const createStore = () => {
  return new Vuex.Store({
    state: {
      posts: [],
      token: null
    },
    mutations: {
      setPosts (state, posts) {
        state.posts = posts
      },
      addPost (state, post) {
        state.posts.push(post)
      },
      editPost (state, editedpost) {
        const postIndex = state.posts.findIndex(
          post => post.id === editedpost
        )
        state.posts[postIndex] = editedpost
      },
      setToken (state, token) {
        state.token = token
      },
      clearToken (state) {
        state.token = null
      }
    },
    actions: {
      nuxtServerInit (vuexContext, context) {
        return context.app.$axios.$get('/posts.json')
          .then(data => {
            const postArray = []
            for (let key in data) {
              postArray.push({ ...data[key], id: key })
            }
            vuexContext.commit('setPosts', postArray)
          })
          .catch(error => console.log(error))
      },
      addPost (vuexContext, post) {
        const createdPost = {
          ...post,
          updatedPost: new Date()
        }
        return this.$axios.$post('/posts.json?auth=' + vuexContext.state.token, createdPost)
          .then(data => {
            console.log(data)
            vuexContext.commit('addPost', { ...createdPost, id: data.name })
          })
          .catch(error => console.log(error))
      },

      editPost (vuexContext, editedpost) {
        return this.$axios.$put(process.env.baseUrl + '/posts/' + editedpost.id + '.json?auth=' + vuexContext.state.token, editedpost)
          .then(result => {
            vuexContext.commit('editPost', editedpost)
          })
          .catch(error => console.log(error))
      },
      setPosts (vuexContext, posts) {
        vuexContext.commit('setPosts', posts)
      },
      authUser (vuexContext, authData) {
        let AuthURL = 'https://identitytoolkit.googleapis.com/v1/accounts:signInWithPassword?key=' + process.env.fbAPIKey
        if (!authData.isLogin) {
          AuthURL = 'https://identitytoolkit.googleapis.com/v1/accounts:signUp?key=' + process.env.fbAPIKey
        }
        return this.$axios.$post(AuthURL,
          {
            email: authData.email,
            password: authData.password,
            returnSecureToken: true
          })
          .then(result => {
            vuexContext.commit('setToken', result.idToken)
            localStorage.setItem('token', result.idToken)
            localStorage.setItem("tokenExpiration", new Date().getTime() + Number.parseInt(result.expiresIn) * 1000); vuexContext.dispatch('setLogoutTimer', result.expiresIn * 1000)
            Cookie.set("jwt", result.idToken)
            Cookie.set("expirationDate", new Date().getTime() + Number.parseInt(result.expiresIn) * 1000);
          })

          .catch(error => console.log(error))
      },

      initAuth (vuexContext, req) {
        let token;
        let expirationDate;
        if (req) {
          if (!req.headers.cookie) {
            return;
          }
          const jwtCookie = req.headers.cookie
            .split(";")
            .find(c => c.trim().startsWith("jwt="));
          if (!jwtCookie) {
            return;
          }
          token = jwtCookie.split("=")[1];
          expirationDate = req.headers.cookie
            .split(";")
            .find(c => c.trim().startsWith("expirationDate="))
            .split("=")[1];
        } else {
          token = localStorage.getItem("token");
          expirationDate = localStorage.getItem("tokenExpiration");
        }
        if (new Date().getTime() > +expirationDate || !token) {
          console.log("No token or invalid token");
          vuexContext.dispatch("logout");
          return;
        }
        vuexContext.commit("setToken", token);
      },
      logout (vuexContext) {
        vuexContext.commit("clearToken");
        Cookie.remove("jwt");
        Cookie.remove("expirationDate");
        if (process.client) {
          localStorage.removeItem("token");
          localStorage.removeItem("tokenExpiration");
        }
      }
    },
    getters: {
      posts (state) {
        return state.posts
      },
      isAuthenticated (state) {
        return state.token != null
      }
    }
  })



}
export default createStore
